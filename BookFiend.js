/**
* Created with BookFiend.
* User: bluechiptony
* Date: 2015-06-28
* Time: 10:25 AM
* To change this template use Tools | Templates.
*/
var myApp = angular.module("myApp", ['ngRoute']);
myApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/search', {
            templateUrl: 'temps/search.html'
        }).when('/login', {
            templateUrl: 'temps/login.html'
        }).when('/signup', {
            templateUrl: 'temps/sign_up.html'
        }).when('/logout', {
            templateUrl: 'temps/logout.html'
        }).otherwise({
            redirectTo: '/login'
        })
    }
]);

//login controller
myApp.controller('loginController', ['user_service', '$http', '$location',
         function(user_service, $http, $location){
             var self = this;
             this.uname='';
             this.pass = '';
             this.login = function(){
                //user_service.setforlog(this.uname, this.pass)
                
                //user_service.set_user_object(self.uname, self.pass);
                //console.log(user_service.get_user_obj());
                //console.log(this.uname);
                //console.log(user_service.get_user_obj().username +" "+ user_service.get_user_obj().password +" "+ user_service.get_user_obj().logged_in);
                
                var upass = this.pass
                //self.docs = {};
                $http({
                    url: 'http://detect-respect.codio.io:8090/users.json?email='+this.uname,
                    method: 'GET',
                    withCredentials: false
                    /*
                    headers: {
                        'Authorization': auth_hash(UserService.get().username, UserService.get().password)
                    }*/
                }).success(function(data, status, headers, config) {
                    
                    var passcheck = data.passcode
                    
                    if(passcheck === upass){
                        //set user object
                        user_service.set_user_object(data.email, data.passcode, data.first_name, data.last_name);
                        console.log(user_service.get_user_obj());
                        $location.path('/search');
                        $location.replace;
                    }else{
                        self.message='Wrong username / password Combination';
                    }
                    
                }).error(function(data, status, headers, config) {
                    console.log(headers);
                    console.log(config);
                    console.log(data)
                    console.log()
                })
             };
             this.stop = function() {
                return false;
            };
            
         }
    ])


//logout controller
myApp.controller('logout', ['user_service', '$http', '$location',
        function(user_service, $http, $location) {
            this.logout = function() {
                console.log('log out called')
                user_service.reset();
                $location.path('/login');
                $location.replace;
            }
            this.logout();
        }
    ]);

//search controller
myApp.controller('searchController', ['user_service', '$http', '$location',
        function(user_service, $http, $location){
            var that = this;
            var self = this;
            this.loggedinstate = user_service.get_user_obj().logged_in;
            this.loggedinuser = user_service.get_user_obj().first_name;
            this.loggedinusername = user_service.get_user_obj().user_name;
            var sec = user_service.get_user_obj().user_name;
            console.log('----------------');
            console.log(user_service.get_user_obj());
            console.log(this.loggedinstate);
            console.log(this.loggedinusername);
            if(this.loggedinstate == false){
                $location.path('/login');
                $location.replace;   
            }
            
            
            this.loginlink = function() {
                if(user_service.get_user_obj().logged_in) {
                    return 'logout'
                } else {
                    return 'login'
                }
            }
            
            self.user_name = "Hi, "+sec;
            this.searchcommence = function(){
                console.log(this.stext);
                $http({
                    url: 'http://detect-respect.codio.io:8090/books?isbn='+this.stext,
                    method: 'GET',
                    
                }).success(function(data, status, headers, config){
                    var book_data = data.items[0]; 
                    
                    self.books = []
                    self.save_button = "Add To Reading List"
                    var book ={
                        title: book_data.volumeInfo.title,
                        publisher: book_data.volumeInfo.publisher,
                        date: book_data.volumeInfo.publishedDate,
                        description: book_data.volumeInfo.description,
                        img: book_data.volumeInfo.imageLinks.thumbnail
                    };
                    self.book = book;
                    self.books.push(book)
                    
                    
                }).error(function(data, status, headers, config){
                    
                });
            }
        }
                                     
    ]);


//register mcontrol
myApp.controller('regController', ['$http', 
        function($http){
            var self = this;
            
            this.commenceRegister = function(){
                var email = self.email;
                var first_name = self.first_name;
                var last_name = self.last_name;
                var password = self.pass_first;
                var url = 'http://detect-respect.codio.io:8090/users?email='+email+'&first_name='+first_name+'&last_name='+last_name+'&password='+password
                $http({
                        url: 'http://detect-respect.codio.io:8090/users?email='+email+'&first_name='+first_name+'&last_name='+last_name+'&password='+password,
                        method: 'PUT'

                }).success(function(data, status, headers, config){
                    console.log(url)
                }).error(function(data, status, headers, config){

                });
            }
            
        }                                  
]);


//factory for user
myApp.factory('user_service', [
    

    function(){
        //status of user object
        var status ={
            logged_in: false,
            user_name: '',
            password:'',
            first_name: '',
            last_name: ''
        };

        return {
            get_user_obj: function(){
                return status;
            },
            
            setforlog: function(uname, pass){
                    status.username = uname;
                    status.password = pass;
                    status.logged_in = true;
                    return status;
            },
            set_user_object: function(uname, password, fname, lname){
                status.user_name = uname;
                status.password = password;
                status.first_name = fname;
                status.last_name = lname
                status.logged_in = true;
                return status;
            },
            reset: function() {
                console.log('user service reset ');
                status = {
                    logged_in: false,
                    user_name: '',
                    password:'',
                    first_name: '',
                    last_name: ''
                    
                }
                return status;
            }
        }
    }
    
    
]);
