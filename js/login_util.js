/**
* Created with BookFiend.
* User: bluechiptony
* Date: 2015-06-27
* Time: 12:38 PM
* To change this template use Tools | Templates.
*/




var L = {
    
    //get element function to return DOM elements by id
    $: function(element_name){
        if(element_name != undefined && typeof element_name == 'string'){
            return document.getElementById(element_name);
        }
    },
    //end of get element method
    
    //add events to required DOM objects
    addEevent: function(obj, evt_type, evt_func){
        if(obj && obj.addEeventListener){
            obj.addEventListener(evt_type, evt_func, false);
        }else if(obj && obj.attachEvent){
            obj.attachEvent('on'+evt_type, evt_func)
        }
    },
    
    removeEvent: function(obj, evt_type, evt_func){
        if(obj && obj.removeEventListener){
            obj.removeEventListener(evt_type, evt_func, false);
        }else if(obj && obj.detachEvent){
            obj.detachEvent('on'+evt_type, evt_func)
        }
    },
    
    makeGetRequest: function(http_method, url){
        //assign httprequest object
        var httpreq;
        var response_text;
        if(window.XMLHttpRequest){
            this.httpreq = new XMLHttpRequest();
        }else{
    
        }
        
        httpreq.open('GET', url);
        httpreq.send();
       
        httpreq.onstatechange = function(){
           if (httpreq.readyState === 4) {
               if (httpreq.status === 200) {
                   //alert(httpRequest.responseText);
                   // console.log(JSON.parse(httpreq.responseText));
                   response_text = httpreq.responseText
               } else {
                   alert('There was a problem with the request.');
               }
           }
        };
    
        return response_text;
    
    }
    //end of get request method
     
    
    
}